package com.enfocat.mvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Llamada {
    private int id;
    private Date fecha;
    private String notas;
    private int idContacto;
    

    public Llamada(int id, String fecha, String notas, int idContacto) {
        this.id = id;
        try {
            SimpleDateFormat sdf =
            new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(fecha);
            this.fecha = date;
        } catch (ParseException e) {}
        
        this.notas = notas;
        this.idContacto = idContacto;
        
    }

    public Llamada( String fecha, String notas, int idContacto) {
        this.id = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(fecha);
            this.fecha = date;
        } catch (ParseException e) {}
        this.notas = notas;
        this.idContacto = idContacto;
        
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", this.fecha, this.notas);
    }

    public String getFecha() {
        try {
            
            String str = new SimpleDateFormat("dd/MM/yyyy").format(fecha);
            return str;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return " ";
    }

    public void setFecha(String fecha) {
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
            this.fecha = date1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
    }

    public int getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(int idContacto) {
        this.idContacto = idContacto;
    }

    

}
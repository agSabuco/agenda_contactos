package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.*;





public class DBDatosLlamada {
    
    private static final String TABLE = "llamadas";
    private static final String KEY = "id"; 
   
    public static Llamada newContacto(Llamada cn){
            String sql; 
            
                sql = String.format("INSERT INTO %s (fecha, notas, idcontact) VALUES (?,?,?)", TABLE);
           
            try (Connection conn = DBConn.getConn();
                    PreparedStatement pstmt = conn.prepareStatement(sql);
                    Statement stmt = conn.createStatement()) {

                //convierte getFecha() en Date para que pueda entrar en MySql
                String[] parts = cn.getFecha().split("/");
                String dia = parts[0]; 
                String mes = parts[1];
                String anyo = parts[2];
                
                String fechaToDate = anyo+"-"+mes+"-"+dia;
                
                pstmt.setString(1, fechaToDate);
                pstmt.setString(2, cn.getNotas());
                pstmt.setInt(3, cn.getIdContacto());

                pstmt.executeUpdate(); 
            
                //usuario nuevo, actualizamos el ID con el recién insertado
                ResultSet rs = stmt.executeQuery("select last_insert_id()"); 
                if (rs.next()) { 
                    cn.setId(rs.getInt(1));
                }
            
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            return cn;
        
    }


  
    public static Llamada getContactoId(int id){
        Llamada cn = null;
        String sql = String.format("select %s,fecha,notas,idcontact from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                cn = new Llamada(
                rs.getInt("id"),
                rs.getString("fecha"),
                rs.getString("notas"),
                rs.getInt("idcontact")
                );
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return cn;
    }
    




    public static boolean updateContacto(Llamada cn){
        boolean updated = false;
        String sql; 

            sql = String.format("UPDATE %s set fecha=?, notas=?, idcontact=?  where %s=%d", TABLE, KEY, cn.getId());
    
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            
            //convierte getFecha() en Date para que pueda entrar en MySql
            String[] parts = cn.getFecha().split("/");
            String dia = parts[0]; 
            String mes = parts[1];
            String anyo = parts[2];

            String fechaToDate = anyo+"-"+mes+"-"+dia;

            pstmt.setString(1, fechaToDate);
            pstmt.setString(2, cn.getNotas());
            pstmt.setInt(3, cn.getIdContacto());
            pstmt.executeUpdate(); 
            updated = true;
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return updated;
    }


    public static boolean deleteContactoId(int id){
        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            deleted=true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return deleted;
    }

    


    public static List<Llamada> getContactos() {

        List<Llamada> listaLlamadas = new ArrayList<Llamada>();  

        String sql = String.format("select id,fecha,notas,idcontact from llamadas"); 
            
        try (Connection conn = DBConn.getConn();
        Statement stmt = conn.createStatement()) { 
    
        ResultSet rs = stmt.executeQuery(sql);
        while (rs.next()) {
            Llamada u = new Llamada(
                        rs.getInt("id"),
                        rs.getString("fecha"),
                        rs.getString("notas"),
                        rs.getInt("idcontact")
                        ); 
                        listaLlamadas.add(u);
            }
    
        } catch (Exception e) { 
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaLlamadas; 
    }

    public static List<Llamada> getLlamadasByContactos(int id) {

        List<Llamada> listaLlamadas = new ArrayList<Llamada>();
        String sql = String.format("select %s,fecha,notas,idcontact from %s where %d=idcontact", KEY, TABLE, id); 

        try (Connection conn = DBConn.getConn();
        Statement stmt = conn.createStatement()) { 
        
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Llamada u = new Llamada(
                            rs.getInt("id"),
                            rs.getString("fecha"),
                            rs.getString("notas"),
                            rs.getInt("idcontact")
                            ); 
                listaLlamadas.add(u);
            }
    
        } catch (Exception e) { 
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaLlamadas; 
    }

}
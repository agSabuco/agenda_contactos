package com.enfocat.mvc;



import java.util.ArrayList;
import java.util.List;

public class DatosLlamada {

    private static List<Llamada> llamadas = new ArrayList<Llamada>();
    private static int ultimoContacto = 0;

    static {
        
            
            llamadas.add(new Llamada(1,"2019-02-26","Nota1",1));
            llamadas.add(new Llamada(2,"2019-05-17","Nota2",1));
        
  
        ultimoContacto=2;
    }



    public static Llamada newContacto(Llamada cn){
        ultimoContacto++;
        cn.setId(ultimoContacto);
        llamadas.add(cn);
        return cn;
    }


    public static Llamada getContactoId(int id){
        for (Llamada cn : llamadas){
            if (cn.getId()==id){
                return cn;
            }
        }
        return null;
    }

    public static boolean updateContacto(Llamada cn){
        boolean updated = false;
        for (Llamada x : llamadas){
            if (cn.getId()==x.getId()){
                //x = cn;
                int idx = llamadas.indexOf(x);
                llamadas.set(idx,cn);
                updated=true;
            }
        }
        return updated;
    }


    public static boolean deleteContactoId(int id){
        boolean deleted = false;
        
        for (Llamada x : llamadas){
            if (x.getId()==id){
                llamadas.remove(x);
                deleted=true;
                break;
            }
        }
        return deleted;
    }

    public static List<Llamada> getContactos() {
        return llamadas;
    }


}
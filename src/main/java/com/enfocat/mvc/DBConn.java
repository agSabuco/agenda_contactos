package com.enfocat.mvc;


import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.*;

public class DBConn {
	
	private static final String URL = "jdbc:mysql://localhost:3306/javacontactos?serverTimezone=UTC";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "alvaro123";

	public static Connection getConn() throws SQLException {
		DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ());
		return DriverManager.getConnection(URL, USERNAME, PASSWORD);
	}
	
}
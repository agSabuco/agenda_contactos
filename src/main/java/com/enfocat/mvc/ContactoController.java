package com.enfocat.mvc;

import java.util.List;

public class ContactoController {

    //si cambiamos por algo diferente a db crea los datos en local, si es db llama a la base de datos
    static final String MODO = "db"; 

    // getAll devuelve la lista completa
    public static List<Contacto> getAll() {
        if (MODO.equals("db")){
            return DBDatos.getContactos();
        }else{
            return Datos.getContactos();
        }
        
    }

   


    // getId devuelve un registro
    public static Contacto getContactById(int id) {
    
        if (MODO.equals("db")){
            return DBDatos.getContactoId(id);
        }else{
            return Datos.getContactoId(id);
        }
        
    }

    // save guarda un Contacto
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Contacto cn) {

        if (MODO.equals("db")){
            if (cn.getId() > 0) {
                DBDatos.updateContacto(cn);
            } else {
                DBDatos.newContacto(cn);
            }
        }else{
            if (cn.getId() > 0) {
                Datos.updateContacto(cn);
            } else {
                Datos.newContacto(cn);
            }
        }

    }

    // size devuelve numero de Contactos
    public static int size() {
        
        if (MODO.equals("db")){
            return DBDatos.getContactos().size();
        }else{
            return Datos.getContactos().size();
        }
    }

    // removeId elimina Contacto por id
    public static void removeId(int id) {
        
        if (MODO.equals("db")){
            DBDatos.deleteContactoId(id);
        }else{
            Datos.deleteContactoId(id);
        }
    }

    // asignaFoto asigna URLde FOTO a alumno por id
    public static void setUrlfoto(int id, String url, String pie) {
        if (url != null){
            ContactoController.getContactById(id).setUrlfoto(url);
        }
        if (pie != null){
            ContactoController.getContactById(id).setPiefoto(pie);
        }
    }

    // asignaFoto asigna URLde FOTO a alumno por id
    public static String getUrlfoto(int id) {
        return ContactoController.getContactById(id).getUrlfoto();
    }

}
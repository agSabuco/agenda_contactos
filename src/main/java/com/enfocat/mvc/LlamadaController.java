package com.enfocat.mvc;

import java.util.List;

public class LlamadaController {

    //si cambiamos por algo diferente a db crea los datos en local, si es db llama a la base de datos
    static final String MODO = "db"; 

    // getAll devuelve la lista completa
    public static List<Llamada> getAll() {
        if (MODO.equals("db")){
            return DBDatosLlamada.getContactos();
        }else{
            return DatosLlamada.getContactos();
        }
        
    }

    // getId devuelve un registro
    public static Llamada getContactById(int id) {
    
        if (MODO.equals("db")){
            return DBDatosLlamada.getContactoId(id);
        }else{
            return DatosLlamada.getContactoId(id);
        }
        
    }

    
    public static List<Llamada> getLlamadaByContactId(int id) {
    
        
        return DBDatosLlamada.getLlamadasByContactos(id);
        
        
    }

    // save guarda un Contacto
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Llamada cn) {

        if (MODO.equals("db")){
            if (cn.getId() > 0) {
                DBDatosLlamada.updateContacto(cn);
            } else {
                DBDatosLlamada.newContacto(cn);
            }
        }else{
            if (cn.getId() > 0) {
                DatosLlamada.updateContacto(cn);
            } else {
                DatosLlamada.newContacto(cn);
            }
        }

    }

    // size devuelve numero de Contactos
    public static int size() {
        
        if (MODO.equals("db")){
            return DBDatosLlamada.getContactos().size();
        }else{
            return DatosLlamada.getContactos().size();
        }
    }

    // removeId elimina Contacto por id
    public static void removeId(int id) {
        
        if (MODO.equals("db")){
            DBDatosLlamada.deleteContactoId(id);
        }else{
            DatosLlamada.deleteContactoId(id);
        }
    }

    
}
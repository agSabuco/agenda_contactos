package com.enfocat.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
// import org.apache.commons.io.output.*;

import com.enfocat.mvc.*;


public class UploadFotoServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String idcontacto_s = request.getParameter("id");

        System.out.println("parametre rebut ");
        System.out.println(idcontacto_s);
        int idcontacto = 0;
        if (idcontacto_s == null) {
            //no recibimos id, debe ser un error... volvemos a index
            response.sendRedirect( request.getContextPath() +"/index.jsp");
            return;
        } else {
             idcontacto = Integer.parseInt(idcontacto_s);
             //igualmente, si id_numerico no es válido (>0), nos vamos
             if (idcontacto<=0) {
                response.sendRedirect( request.getContextPath() +"/index.jsp");
                return;   
             }
        }
    
        // solo llegamos aquí si tenemos un idcontacto válido! seguimos...
        Contacto cn = ContactoController.getContactById(idcontacto);
        
    
        File file ;
        int maxFileSize = 10000 * 1024; // 10 mb tamaño máximo en disco
        int maxMemSize = 5000 * 1024; // 5 mb tamaño maximo en memoria
        
        String urlImagen = null;
    
        String pathImagenes = "/uploads"; // carpeta donde guardaremos las imágenes
        String urlBase = request.getContextPath() + pathImagenes;
    
        // archivoDestino contendrá el nombre del archivo que crearmos
        String archivoDestino = null;
        String pieFoto = null;
    
        //getRealPath nos da la ubicación en disco de la carpeta /uploads
        //necesaria para guardar el archivo subido
        //podria ser cualquier carpeta con acceso de escritura para el usuario "tomcat" en linux
        //o cualquier carpeta no protegida de windows
        //en cualquier caso, las "/" son estilo linux, ex: "c:/usuarios/nombre/uploads"
        //en este caso suponemos que es la carpeta uploads dentro de webapp
        // IMPORTANTE añadir el "/" al final!
    
        //CAMBIO EN SERVLET!!!
        //String realPath = pageContext.getServletContext().getRealPath(pathImagenes)+"/";
        ServletContext context = request.getServletContext();
        String realPath = context.getRealPath(pathImagenes)+"/";
        
    
        // verificamos si estamos recibiendo imagen
        // en dos sentidos: si es POST y si es tipo multipart
        String contentType = request.getContentType();
        if ("POST".equalsIgnoreCase(request.getMethod()) && contentType.indexOf("multipart/form-data") >= 0) {
    
            System.out.println("entrem...");
          DiskFileItemFactory factory = new DiskFileItemFactory();
          factory.setSizeThreshold(maxMemSize);
          //por si hiciese falta, indicamos la carpeta tmp
          factory.setRepository(new File(System.getProperty( "java.io.tmpdir" )));
          ServletFileUpload upload = new ServletFileUpload(factory);
          upload.setSizeMax( maxFileSize );
          try{ 
            // ojo, fileItems no sólo incluye archivos, podrian ser también
            // campos INPUT tales como el pie de foto
             List<FileItem> formItems = upload.parseRequest(request);
           
            if (formItems != null && formItems.size() > 0) {
                
                for (FileItem fi : formItems) {
    
                    
                    
                    if ( fi.isFormField() )  {
                        // hemos recibido un campo de formulario, NO FILE
                        // pero no podemos utilizar getParameter...
                        // podemos utilizar un switch...case... para verificar el "fieldName"
                        String fieldname = fi.getFieldName();
                        String fieldvalue = fi.getString();
                        //System.out.println(fieldname);
                        //System.out.println(fieldvalue);
                        if (fieldname.equals("pie")){
                            System.out.println("Pie assignat");
                            pieFoto = fieldvalue;
                        }
    
                    }else{
    
                        if (fi.getSize()>0){
                            System.out.println("File assignat");

                            //si no es formfiueld es un FILE
                            //filename es el nombre del archivo que se ha subido
                            String fileName = fi.getName();
                                    
                            // valores que no utilizamos: 
                            //   String fieldName = fi.getFieldName();
                            //   boolean isInMemory = fi.isInMemory();
                            //   long sizeInBytes = fi.getSize();
                            
                            // creamos puntero a archivo ubicado en carpeta real del disco
                            // más nombre de archivoDestino (igual al que se ha subido en este caso)
                            // IMPORTANTE: podríamos cambiar aquí el nombre de archivo destino
                            archivoDestino = fileName;
                            file = new File( realPath + archivoDestino ); 
                            
                            //qué pasa si archivo ya existe? nos inventamos un prefijo
                            //foto.jpg, 1_foto.jpg, 2_foto.jpg...
                            int t = 0;
                            String archivoTemp = archivoDestino;
                            while (file.exists() && !file.isDirectory()){
                                archivoTemp = String.valueOf(++t)+"_"+archivoDestino;
                                file = new File( realPath + archivoTemp ); 
                            }
                            archivoDestino=archivoTemp;
    
                            // escribimos archivo en disco:
                            fi.write( file ) ;
    
                            //getContextPath() devuelve nombre app, en este caso: "/contactos"
                            //guardamos el nombre de la imagen tal como lo requerirá html
                            urlImagen = urlBase + "/" + archivoDestino;
                            //guardamos nombre img
                            //ContactoController.getUrlfoto(idcontacto);
                        }else{
                            System.out.println("NO File!");
                        }
                    } 
    
          
                }
              }
    
          }catch(Exception ex) {
              //error, mostramos mensaje y redirigimos a listado...
             System.out.println(ex);
           
    
          }
    
    
            //importante, es posible que tanto archivoDestino como pieFoto sean null, 
            //ContactoController deberá gestionarlo...
            ContactoController.setUrlfoto(idcontacto, archivoDestino, pieFoto);
    
            response.sendRedirect( request.getContextPath() +"/contacto/list.jsp");
             return;
    
        }
    
    }
}
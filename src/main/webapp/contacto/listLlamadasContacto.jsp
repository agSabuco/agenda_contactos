<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>
<%@page import="java.util.Date" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.List" %>


<%

    List<Llamada> cn = null;
    boolean datos_guardados=false;

    //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");
    String nombre = request.getParameter("nombre");
    
    if (id==null){
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/contactos");
        //IMPORTANTE! después de sendRedirect poner un RETURN!!!
        return;
    }else{
            // hemos recibido un GET, solo tenemos un id
            // pedimos los datos del contacto para mostrarlos en el formulario de edifión
            cn = LlamadaController.getLlamadaByContactId(Integer.parseInt(id));
            if (cn==null) {
                    response.sendRedirect("/contactos");
                    return;
            } 
        
    }
%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>LlamadasApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>
<body>


<%@include file="/menu.jsp"%>

<div class="container">

<div class="row">
  <div class="col">
    <h1>Listado de llamadas de <%= nombre %></h1>
  </div>
</div>

<div class="row">
<div class="col">

<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Fecha</th>
      <th scope="col">Notas</th>
    
        <th scope="col"></th>
        <th scope="col"></th>
    
    </tr>
  </thead>
  <tbody>

   <% for (Llamada currentLlamada : cn) { %>
        <tr>
        <th scope="row"><%= currentLlamada.getId() %></th>
        <td><%= currentLlamada.getFecha() %></td>
        <td><%= currentLlamada.getNotas() %></td>
        <td>
          <a href="/contactos/llamadas/edita.jsp?id=<%= currentLlamada.getId() %>" class="btn btn-primary a-btn-slide-text">
            <i class="far fa-edit"></i>
            <span><strong>Edita</strong></span>            
          </a>
        </td>
        <td>
          <a href="/contactos/llamadas/elimina.jsp?id=<%= currentLlamada.getId() %>" class="btn btn-danger a-btn-slide-text">
              <i class="fas fa-trash-alt"></i>
              <span><strong>Elimina</strong></span>            
          </a>
        </td>
      </tr>
    <% } %>
  </tbody>
</table>

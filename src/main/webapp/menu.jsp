<%@page contentType="text/html;charset=UTF-8" %>


<nav class="navbar navbar-expand-lg  navbar-dark bg-primary">
    <a class="navbar-brand" href="/contactos/contacto/list.jsp">Contactos</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

    
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Opciones
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/contactos/contacto/list.jsp">Listado Contactos</a>
            <a class="dropdown-item" href="/contactos/llamadas/list.jsp">Listado Llamadas</a>
            <a class="dropdown-item" href="/contactos/contacto/crea.jsp">Nuevo Contacto</a>
            <a class="dropdown-item" href="/contactos/llamadas/crea.jsp">Nueva Llamada</a>
          </div>
        </li>

      </ul>

    </div>
  </nav>
  

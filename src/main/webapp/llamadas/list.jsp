<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>
<%@page import="java.util.Date" %>
<%@page import="java.text.SimpleDateFormat" %>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>LlamadasApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>
<body>


<%@include file="/menu.jsp"%>

<div class="container">

<div class="row">
  <div class="col">
    <h1>Listado de llamadas</h1>
  </div>
</div>

<div class="row">
<div class="col">

<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Fecha</th>
      <th scope="col">Notas</th>
    
        <th scope="col"></th>
        <th scope="col"></th>
    
    </tr>
  </thead>
  <tbody>

   <% for (Llamada cn : LlamadaController.getAll()) { %>
        <tr>
        <th scope="row"><%= cn.getId() %></th>
        <td><%= cn.getFecha() %></td>
        <td><%= cn.getNotas() %></td>
        <td>
          <a href="/contactos/llamadas/edita.jsp?id=<%= cn.getId() %>" class="btn btn-primary a-btn-slide-text">
            <i class="far fa-edit"></i>
            <span><strong>Edita</strong></span>            
          </a>
        </td>
        <td>
          <a href="/contactos/llamadas/elimina.jsp?id=<%= cn.getId() %>" class="btn btn-danger a-btn-slide-text">
              <i class="fas fa-trash-alt"></i>
              <span><strong>Elimina</strong></span>            
          </a>
        </td>
      </tr>
    <% } %>
  </tbody>
</table>

<a href="/contactos/llamadas/crea.jsp" class="btn btn-success">
  <i class="fas fa-phone-alt"></i>
  <span><strong>Nueva llamada</strong></span>
</a>
</div>
</div>

</div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/contactos/js/scripts.js"></script>
</body>
</html>

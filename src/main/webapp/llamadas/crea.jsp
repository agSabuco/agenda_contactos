<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>
<%@page import="java.util.Date" %>
<%@page import="java.text.SimpleDateFormat" %>


<%


    boolean datosOk;

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo contacto
    
        //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
        request.setCharacterEncoding("UTF-8");

        String fecha = request.getParameter("fecha");
        String notas = request.getParameter("notas");
        
        String nombre = request.getParameter("encodings");
        int idcontact = Integer.parseInt(nombre);
        


        // aquí verificaríamos que todo ok, y solo si todo ok hacemos SAVE, 
        // por el momento lo damos por bueno...
        datosOk=true;
        if (datosOk){
            
            Llamada newCall = new Llamada(fecha, notas, idcontact);
            LlamadaController.save(newCall);
            
            
            // nos vamos a mostrar la lista, que ya contendrá el nuevo contacto
            response.sendRedirect("/contactos/llamadas/list.jsp");
            return;
        }
    }


%>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>
<body>

<%@include file="/menu.jsp"%>

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Nueva Llamada</h1>
        </div>
    </div>


<div class="row">
<div class="col-md-8">

<form id="formcrea" action="#" method="POST">
    <div class="form-group">
        <label for="fechaInput">Fecha</label>
        <input  name="fecha" type="date" class="form-control" id="fechaInput" required>
    </div>

    <div class="form-group">
        <label for="notasInput">Notas del contacto</label>
        <input  name="notas"  type="text" class="form-control" id="notasInput">
    </div>

    <div class="form-group">
        <label for="encodings">Llamada para:</label>
        <select id="encodings" name="encodings" class="custom-select">

        <% for (Contacto cn : ContactoController.getAll()) { %>
            <option value="<%= cn.getId() %>"><%= cn.getNombre() %></option>
        <% } %>

        </select>
    </div>

    <button type="submit" class="btn btn-primary">Guardar</button>
</form>


</div>
</div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

</body>
</html>
